<?php /*include('../../lib/config.php');*/
?>
<!DOCTYPE html><html lang="en">
<head>
    <meta charset="utf-8">
    <title>VI - Work Order Form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- ==========CSS=========== -->
<!--##BOOTSTRAP_3.3_custom-for-workorder(CSS)-->
    <link href="src/bootstrap3-custom-for-workorder/css/bootstrap.min.css" rel="stylesheet">
<!--##BOOTSTRAP_Datepicker_1.4.0(CSS)-->
    <link href="src/bs-datepicker-1.4.0/css/bootstrap-datepicker3.min.css" rel="stylesheet">
<!--##USER(CSS)-->
    <link href="src/wo-slim.css" rel="stylesheet">
    <link href="src/wo-slim-print.css" rel="stylesheet">

</head>
<body>

<form class="page-container">
    <div class="container">

        <div class="row bordered">
            <h1 class="col-xs-5">Production Work Order</h1>
            <div class="col-xs-7">Signatures Required:
                <div class="btn-group btn-group-sm" data-toggle="buttons">
                    <label class="btn btn-default active">
                        <input type="radio" name="options" id="sigsreqd1" autocomplete="off" checked>
                        &nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;
                    </label>
                    <label class="btn btn-default">
                        <input type="radio" name="options" id="sigsreqd2" autocomplete="off">
                        &nbsp;&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;&nbsp;
                    </label>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="input-group input-group-sm">
                    <div class="input-group-addon">JOB#</div>
                    <input id="invoice-num" name="invoice-num" class="form-control" type="text">
                </div>
            </div>
        </div><!-- end: ROW (header)-->


        <div class="row"><!-- ROW (2 COLS) -->
            <!-- COL LEFT -->
            <div class="col-xs-8 column">
                <div class="row">
                    <div class="col-xs-16 bordered vert-separate">
                        <div class="row">
                            <div class="col-sm-6">Customer:</div>
                            <div class="col-sm-10">
                                <input id="customer" name="customer" type="text" class="form-control input-sm">
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-sm-6">Project Name:</div>
                            <div class="col-sm-10">
                                <input id="projectname" name="projectname" type="text" class="form-control input-sm">
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-sm-6">Address:</div>
                            <div class="col-sm-10">
                                <textarea type="text" class="form-control input-sm textarea"></textarea>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-sm-6">Sales Rep:</div>
                            <div class="col-sm-10">
                                <input id="salesrep" name="salesrep" type="text" class="form-control input-sm">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-16 bordered vert-separate">
                        <div class="row">
                            <h2>Contact:</h2>

                            <div class="col-sm-6">Name:</div>
                            <div class="col-sm-10">
                                <input id="contact" name="contact" type="text" class="form-control input-sm">
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-sm-6">Phone #:</div>
                            <div class="col-sm-10">
                                <input id="phonenum" name="phonenum" type="text" class="form-control input-sm">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-xs-16 bordered vert-separate">
                        <div class="row">
                            <h2>Input:</h2>

                            <div class="col-sm-7">
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Scans</div>
                                    <input id="invoice-num" name="invoice-num" class="form-control" type="text">
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Supplied Media</div>
                                    <input id="invoice-num" name="invoice-num" class="form-control" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox2" value="option2"> Email
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox1" value="option1"> Retrieval
                                </label>
                            </div>
                            <div class="col-sm-8">
                                Retrieval Num or Date
                                <input class="form-control" data-provide="datepicker" data-date-force-parse="false">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- COL RIGHT -->
            <div class="col-xs-8">
                <div class="row">
                    <div class="col-xs-15 col-xs-offset-1 bordered vert-separate">
                        <div class="row">
                            <div class="col-sm-7">In Production Ship Proofs</div>
                            <div class="col-sm-9">
                                 <input class="form-control datepicker" data-provide="datepicker">
                                 <input class="form-control datepicker" data-provide="datepicker">
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-sm-6">Ship Job:</div>
                            <div class="col-sm-10">
                                <input id="projectname" name="projectname" type="text" class="form-control input-sm">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-15 col-xs-offset-1 bordered vert-separate">

                        <div class="row">
                            <h2>Proof Address:</h2>

                            <div class="form-group">
                                <div class="col-sm-15 col-sm-offset-1">
                                    <textarea type="text" class="form-control input-sm textarea"></textarea>
                                    <br/>
                                </div>

                                <div class="col-sm-15 col-sm-offset-1">
                                    <textarea type="text" class="form-control input-sm textarea"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: ROW (2 columns)-->

        <div class="row">
            <table id="mainTable" border="1">
                <tr style="font-weight:bold; text-align: right;">
                    <th class="">Vinyl</th>
                    <th class="">Lam</th>
                    <th class="">Name</th>
                    <th class="">Description</th>
                    <th class="">Unit_Price</th>
                    <th class="">Total_Price</th>
                    <th class="">Part_#</th>
                    <th class="">Width</th>
                    <th class="">Height</th>
                    <th class="">Qty</th>
                </tr>
                <?php for ($i = 0; $i < 15; $i++) { ?>
                <tr style="font-weight:bold; text-align: right;">
                        <td data-what="main-vinyl-<?php echo $i; ?>">&nbsp;</td>
                        <td data-what="main-lam-<?php echo $i; ?>">&nbsp;</td>
                        <td data-what="main-name-<?php echo $i; ?>">&nbsp;</td>
                        <td data-what="main-desc-<?php echo $i; ?>">&nbsp;</td>
                        <td data-what="main-uniprice-<?php echo $i; ?>">&nbsp;</td>
                        <td data-what="main-totprice-<?php echo $i; ?>">&nbsp;</td>
                        <td data-what="main-partnum-<?php echo $i; ?>">&nbsp;</td>
                        <td data-what="main-width-<?php echo $i; ?>">&nbsp;</td>
                        <td data-what="main-height-<?php echo $i; ?>">&nbsp;</td>
                        <td data-what="main-qty-<?php echo $i; ?>">&nbsp;</td>
                    </tr>
                <?php } ?>
            </table>

        </div>
        <!-- Submit --><br/>
        <div class="row">
            <div class="text-center">
                <button id="submit-wkorder" name="submit-wo" class="btn btn-primary">
                    Submit Production Work Order
                </button>
                <div>[VISUAL IMPRESSIONS]</div>
            </div>
        </div>
    </div>

<br/><br/><br/>
</form>
<!-- ==========JS=========== -->
<!--##JQUERY-->
<script src="src/jquery-1.11.3.min.js" type="text/javascript"></script>
<!--##BOOTSTRAP_3.3_custom-for-workorder-->
<script src="src/bootstrap3-custom-for-workorder/js/bootstrap.min.js"></script>
<!--##BOOTSTRAP_FormHelper_Datepicker_1.4.0-->
<script src="src/bs-datepicker-1.4.0/js/bootstrap-datepicker.min.js"></script>
<!--##editable-table(JS)-->
<script src="src/editable-table-master/mindmup-editabletable.js"></script>
<!--##Autogrow-Jason Edelman-->
<script src="src/autogrow-jedelman/autogrow.min.js"></script>
<!--##USER(JS)-->
<script src="src/wo-slim.js"></script>

<script>
    /* ******* editable-table init ******* */
//    $('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();
    $('#mainTable').editableTableWidget();


    /* ******* datepicker init ******* */   /* http://bootstrap-datepicker.readthedocs.org/en/latest/ */
    $('.datepicker').datepicker({
        format: 'mm/dd/yyyy',
        startDate: 'today'
    });
    $('.datepicker2').datepicker({
        forceParse: 'false'
    });


    /* ******* textarea auto-grow ******* */
    $('.textarea').autogrow();

</script>


</body>
</html>