<?php  /*include('../../lib/config.php');*/
//include('config.php');?>
<!DOCTYPE html><html lang="en">
<head>
    <meta charset="utf-8">
    <title>VI - Work Order Form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <meta name="author" content="John Brittain III">
    <meta name="robots" content="NOINDEX,NOFOLLOW">

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/cp/styles/imgs/favicon144x144.gif">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/cp/styles/imgs/favicon114x114.gif">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/cp/styles/imgs/favicon72x72.gif">
    <link rel="apple-touch-icon-precomposed" href="/cp/styles/imgs/favicon57x57.gif">
    <link rel="shortcut icon" href="/cp/styles/imgs/favicon.gif">

    <!--    <link href="styles/vi-bs.css" type="text/css" rel="stylesheet">-->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <!--    <link href="styles/vi-responsive.css" type="text/css" rel="stylesheet">-->
    <!--    <link href="styles/plugins/jschrs_modal/modal.css" type="text/css" rel="stylesheet">-->
    <!--    <link href="styles/plugins/bootstrap-datepicker-1.2.0/css/datepicker.css" type="text/css" rel="stylesheet">-->
    <!--	<link href="<?php /*echo $Sets['path']; */ ?>styles/vi-bs.css" type="text/css" rel="stylesheet">
	<link href="<?php /*echo $Sets['path']; */ ?>styles/vi-responsive.css" type="text/css" rel="stylesheet">
	<link href="<?php /*echo $Sets['path']; */ ?>styles/plugins/jschrs_modal/modal.css" type="text/css" rel="stylesheet">
	<link href="<?php /*echo $Sets['path']; */ ?>styles/plugins/bootstrap-datepicker-1.2.0/css/datepicker.css" type="text/css" rel="stylesheet">
-->

    <style type="text/css">
        h1 {
            font-size: 18px;
            line-height: 20px;
            margin: 0;
            padding: 0;
        }

        .no-border td {
            border: 0;
        }

        .chkbox {
            border: 1px solid #000;
        }

        .box {
            border: 1px solid #000;
            width: 110px;
            height: 15px;
        }

        .text-box {
            width: 90% !important;
            margin: 0 !important;
        }

        .pull-right .text-box {
            width: 60% !important;
        }

        .comments {
            border: 1px solid #ddd;
        }
.bordered {border:1px solid #000;
border-radius: 4px; }
        @media print {
            * {
                text-shadow: none !important;
                color: #000 !important; /* Black prints faster : h5bp.com/s*/
                background: transparent !important;
                box-shadow: none !important;
            }

            /*Don't show links for images, or javascript/internal links*/
            .ir a:after, a[href^="javascript:"]:after, a[href^="#"]:after {
                content: "";
            }

            thead {
                display: table-header-group; /* h5bp.com/t */
            }

            tr, img {
                page-break-inside: avoid;
            }
        }
    </style>
</head>
<body>

<form class="page-container">
    <div class="container"><br/>

            <fieldset class="row bordered">

                    <h1 class="col-xs-4">Production Work Order</h1>

                    <div class="col-xs-5">
                        <label class="control-label" for="sigsreqd">Signatures Required:</label>
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-xs btn-default active">
                                <input type="radio" name="sigsreqd" id="sigsreqd1" autocomplete="off" checked>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </label>
                            <label class="btn btn-xs btn-default">
                                <input type="radio" name="sigsreqd" id="sigsreqd2" autocomplete="off">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </label>
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <div class="input-group">
                            <span class="input-group-addon">JOB#</span>
                            <input id="prependedtext" name="invoice-num" class="form-control" type="text">
                        </div>
                    </div>

            </fieldset><!-- end: ROW (header)--><br/>


        <div class="row"><!-- ROW (2 COLS) -->

            <div class="col-xs-6 column">
                <div class="row">
                    <form class="form-inline">
                        <div class="form-group">
                            <label class="col-xs-4 control-label" for="textinput">Customer</label>

                            <div class="col-xs-8">
                                <input id="Customer" name="textinput" type="text" class="form-control input-sm"
                                       placeholder="placeholder">
                            </div>
                            <label class="col-xs-4 control-label" for="projectname">Project Name</label>

                            <div class="col-xs-8">
                                <input id="projectname" name="projectname" type="text" class="form-control input-sm">
                            </div>
                            <label class="col-xs-4 control-label" for="address">Address</label>

                            <div class="col-xs-8">
                                <input id="address" name="address" type="text" class="form-control input-sm">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ProjectName">Project Name</label>
                            <input type="text" class="form-control" id="ProjectName" placeholder="Project Name">
                        </div>
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox1" value="option1"> 1
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox2" value="option2"> 2
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox3" value="option3"> 3
                        </label>
                    </form>
                </div>
                <table class="table table-condensed table-bordered">
                    <tr>
                        <td class="col-xs-2">Customer</td>
                        <td class="col-xs-2 editable" data-what="address" data-type=""></td>
                    </tr>
                    <tr>
                        <td class="col-xs-2">Project Name</td>
                        <td class="col-xs-2 editable" data-what="address" data-type=""></td>
                    </tr>
                    <tr>
                        <td class="col-xs-2">Address</td>
                        <td class="col-xs-2 editable" data-what="address" data-type=""></td>
                    </tr>
                    <tr>
                        <td class="col-xs-2">Sales Rep</td>
                        <td class="col-xs-2 editable" data-what="address" data-type=""></td>
                    </tr>
                    <tr>
                        <td class="col-xs-2">Contact</td>
                        <td class="col-xs-2 editable" data-what="address" data-type=""></td>
                    </tr>
                    <tr>
                        <td class="col-xs-2">Phone Number</td>
                        <td class="col-xs-2 editable" data-what="address" data-type=""></td>
                    </tr>
                    <tr>
                        <td class="col-xs-2">Scans</td>
                        <td class="col-xs-2 editable" data-what="address" data-type=""></td>
                    </tr>
                    <tr>
                        <td class="col-xs-2">Supplied Media</td>
                        <td class="col-xs-2 editable" data-what="address" data-type=""></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="col-xs-2 checkbox chkbox pull-left" data-what="232-chkbox"></div>
                            Retrieval
                        </td>
                        <td>
                            <div class="col-xs-2 checkbox chkbox pull-left" data-what="313-chkbox"></div>
                            Email
                        </td>
                    </tr>
                    <tr>
                        <td class="editable" data-what="date" data-type="textarea">
                            <div class="pull-right">
                                Job #
                                <col-xs- class="editable invoice-num" data-what="invoice-number">[100]</col-xs->
                                <br/>
                                DATE:
                                <col-xs- class="editable date-picker" data-what="date" data-type="date">[PICK THE DATE]
                                </col-xs->
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

            <!-- COL RIGHT -->
            <div class="col-xs-6 column">
                <table class="table table-condensed">
                    <tr>
                        <td class="editable" data-what="client-info" data-type="location">
                            <strong>CLIENT INFO:</strong><br>
                            [Name]<br>[Company Name]<br>[Street Address]<br>[City, ST ZIP Code]<br>[Phone Number]
                        </td>
                        <td class="editable" data-what="shipping-info" data-type="location">
                            <strong>SHIPPING INFO:</strong><br>
                            [Name]<br>[Company Name]<br>[Street Address]<br>[City, ST ZIP Code]<br>[Phone Number]
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <!-- end: ROW (2 columns)-->


        <div class="row">
            <table class="table table-condensed no-border">
                <tr>
                    <td>
                        <strong>JOB DESCRIPTION/SPECIAL COMMENTS </strong>
                    </td>
                </tr>
                <tr>
                    <td class="comments editable" data-what="job-comments" data-type="textarea">comments here..</td>
                </tr>
            </table>
        </div>

        <div class="row-fluid">
            <div class="col-xs-10 offset1">
                <div class="row-fluid">
                    <table class="table table-bordered assigned">
                        <thead>
                        <tr>
                            <th class="col-xs-3">ASSIGNED TO:</th>
                            <th class="col-xs-3">START DATE</th>
                            <th class="col-xs-3">DUE DATE</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="editable" data-what="assigned-to-1" data-type="textarea">&nbsp;</td>
                            <td class="editable" data-what="assigned-start-1" data-type="date">&nbsp;</td>
                            <td class="editable" data-what="assigned-due-1" data-type="date">&nbsp;</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row-fluid">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th class="col-xs-1">Vinyl</th>
                    <th class="col-xs-1">Lam</th>
                    <th class="col-xs-2">Name</th>
                    <th class="col-xs-2">Description</th>
                    <th class="col-xs-1">Unit Price</th>
                    <th class="col-xs-1">Total Price</th>
                    <th class="col-xs-1">Part #</th>
                    <th class="col-xs-1">W</th>
                    <th class="col-xs-1">H</th>
                    <th class="col-xs-1">Qty</th>
                </tr>
                </thead>
                <tbody>
                <?php for ($i = 0; $i < 15; $i++) { ?>
                    <tr>
                        <td class="editable" data-what="main-vinyl-<?php echo $i; ?>">&nbsp;</td>
                        <td class="editable" data-what="main-lam-<?php echo $i; ?>">&nbsp;</td>
                        <td class="editable" data-what="main-name-<?php echo $i; ?>">&nbsp;</td>
                        <td class="editable" data-what="main-desc-<?php echo $i; ?>" data-type="textarea">&nbsp;</td>
                        <td class="editable" data-what="main-uniprice-<?php echo $i; ?>">&nbsp;</td>
                        <td class="editable" data-what="main-totprice-<?php echo $i; ?>">&nbsp;</td>
                        <td class="editable" data-what="main-partnum-<?php echo $i; ?>">&nbsp;</td>
                        <td class="editable" data-what="main-width-<?php echo $i; ?>">&nbsp;</td>
                        <td class="editable" data-what="main-height-<?php echo $i; ?>">&nbsp;</td>
                        <td class="editable" data-what="main-qty-<?php echo $i; ?>">&nbsp;</td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</form>


<!-- Submit -->
<div class="form-group">
    <div class="text-center">
        <button id="submit-workorder" name="submit-workorder"
                class="btn btn-primary text-center">Submit Production Work Order
        </button>
        <div>[VISUAL IMPRESSIONS]</div>
    </div>
</div>
<br/><br/><br/>


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
<script>window.jQuery || document.write('<script src="styles/jquery.1.10.2.min.js"><\/script>')</script>
<!--<script type="text/javascript" src="styles/jquery.form.js"></script>-->
<!--<script type="text/javascript" src="/cp/styles/plugins/bootstrap-2.3.2/js/bootstrap.js"></script>-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<!--<script type="text/javascript" src="/cp/styles/plugins/jschrs_modal/bootstrap-modalmanager.js"></script>-->
<!--<script type="text/javascript" src="/cp/styles/plugins/jschrs_modal/bootstrap-modal.js"></script>-->

<!--<script type="text/javascript" src="styles/plugins/autoGrow.js"></script>-->
<!--<script type="text/javascript" src="styles/plugins/bootstrap-datepicker-1.2.0/js/bootstrap-datepicker.js"></script>-->
<script type="text/javascript">


    $(function () {
        var $editing = false, box = {}, boxes = [], editable = $('.editable')

        $('#save_puur').on('click', function () {
            editable.each(function () {
                var th = $(this), what = th.data('what'), input = th.find('.text-box'), val
                if (input.length) {
                    if (what == 'r-date') {
//					alert('gotta add the sups back to the date!')
                        val = input.val()
                        val = val.replace(/(\d+)+(st|nd|rd|th)([^\w\d]|$)/g, "$1<sup>$2</sup>$3")
                    } else {
                        val = input.val()
                    }
                    th.html(val)
                }
                box = {
                    what: what,
                    data: th.html()
                }
                boxes.push(box)
            })
            $.post('spuur.gen.php', {values: boxes}, function (r) {
                $('#results').html(r)
                window.location.href = '<?php echo $_SERVER['PHP_SELF']; ?>?download'
            })
//		console.log(boxes)
        })

        editable.on('click', function () {
            var th = $(this), what = th.data('what'), inner = th.html().replace('"', "'"), type;
            if (th.find('.text-box').length) {
//			alert('has input')
            } else {
                if ($editing != false) {
                    // they're currently editing a line so we gonna update that line.
                    var input = $('.container .text-box');
                    if (input.length > 0) {
                        var val = input.val(), parent = input.parent(), type;
                        //input.datepicker('hide').datepicker('remove')
                        if (type = th.data('type') && type == 'location') { //if(input.hasData('type') && input.data('type') == 'location'){
                            alert('workin on it!');
                        } else {
                            parent.html(val.replace(/\n/g, "<br>"))
                        }
                    }
                }
                $editing = what
                if (type = th.data('type')) { //.length > 0){
                    if (type == 'textarea') {
                        th.html('<textarea rows="2" class="text-box">' + inner.replace(/<br>/g, "\n") + '</textarea>')
                        th.find('textarea').blur().focus()
                        $('textarea').autoGrow();
                    } else if (type == 'date') {
                        th.html('<input type="text" value="' + inner + '" class="text-box">')
                        th.find('input').datepicker({
                            format: "D, M m, yyyy",
                            startView: 1,
                            autoclose: true,
                            todayHighlight: true
                        }).datepicker('show');
                    } else if (type == 'location') {
                        $('#contact-info-modal').find('.moday-body').html('<div class="progress progress-striped active"><div class="bar"></div></div>').end().modal();
                    } else {
                        alert(type + ' - not set up yet..')
                    }
                } else {
                    th.html('<input type="text" value="' + inner + '" class="text-box">')
                    th.find('input').blur().focus()
                }
            }
        }).each(function (ind, val) {
            $(this).data('index', ind)
        })
        $(document).delegate('.text-box', 'keypress', function (e) {
            var th = $(this), code = e.keyCode || e.which
            if (code == 13 && th.prop("tagName") == 'TEXTAREA') {

            } else if (code == 13 || code == 9) {
                e.preventDefault();
                var th = $(this), val = th.val(), par = th.parent('.editable'), what = par.data('index')

                par.html(val.replace(/\n/g, "<br>"))
                $editing = false
                $('.editable').eq(parseInt(what) + 1).trigger('click')
            }
        })

        /* was trying to clear the input field if found.. but freezes everything..
         .on('click', function(){
         var input = $('input')
         if(input.length){ // input found
         // lets hide that input..
         input.parent().html(input.val())
         }
         })
         */
    })
</script>
</body>
</html>