<!DOCTYPE html><html lang="en">
<head>
    <meta charset="utf-8">
    <title>VI - Work Order Form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!--JQUERY-->
    <script src="src/jquery-1.11.3.min.js" type="text/javascript"></script>
<!--    <script src="src/jquery-2.1.0.min.js" type="text/javascript"></script>-->
<!--BOOTSTRAP_3.3-->
    <link href="src/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="src/bootstrap/js/bootstrap.min.js"></script>
<!--JQUERY-UI-->
<!--    <link rel="stylesheet" href="src/jquery-ui-1.11.4/jquery-ui.min.css" type="text/css" media="screen">-->
<!--    <script src="src/jquery-ui-1.11.4/jquery-ui.min.js"></script>-->
<!--JQUERY-UI-CUSTOM-->
<!--    <link rel="stylesheet" href="src/jquery-ui-1.11.4.custom/jquery-ui.min.css" type="text/css" media="screen">-->
<!--    <script src="src/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>-->
<!--.....USER.....-->
    <link href="wo-slim.css" rel="stylesheet">
    <script src="wo-slim.js"></script>
    <script src="src/editable-table-master/mindmup-editabletable.js"></script>

</head>
<body>

<form class="page-container">
    <div class="container">

        <div class="row bordered">
            <h1 class="col-xs-4">Production Work Order</h1>
            <div class="col-xs-5">Signatures Required:
                <div class="btn-group btn-group-sm" data-toggle="buttons">
                    <label class="btn btn-default active">
                        <input type="radio" name="options" id="sigsreqd1" autocomplete="off" checked>
                        &nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;
                    </label>
                    <label class="btn btn-default">
                        <input type="radio" name="options" id="sigsreqd2" autocomplete="off">
                        &nbsp;&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;&nbsp;
                    </label>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="input-group input-group-sm">
                    <div class="input-group-addon">JOB#</div>
                    <input id="invoice-num" name="invoice-num" class="form-control" type="text">
                </div>
            </div>
        </div><!-- end: ROW (header)--><br/>


        <div class="row"><!-- ROW (2 COLS) -->
            <!-- COL LEFT -->
            <div class="col-xs-6 column">
                <div class="row">
                    <div class="col-sm-12 bordered vert-separate">
                        <div class="row">
                            <div class="col-sm-4">Customer:</div>
                            <div class="col-sm-8">
                                <input id="customer" name="customer" type="text" class="form-control input-sm">
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-sm-4">Project Name:</div>
                            <div class="col-sm-8">
                                <input id="projectname" name="projectname" type="text" class="form-control input-sm">
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-sm-4">Address:</div>
                            <div class="col-sm-8">
                                <textarea id="contact" rows="3" type="text" class="form-control input-sm"></textarea>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-sm-4">Sales Rep:</div>
                            <div class="col-sm-8">
                                <input id="salesrep" name="salesrep" type="text" class="form-control input-sm">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 bordered vert-separate">
                        <div class="row">
                            <em style="text-decoration: underline;">Contact:</em>
                            <div class="col-sm-4">Name:</div>
                            <div class="col-sm-8">
                                <input id="contact" name="contact" type="text" class="form-control input-sm">
                            </div>

                            <div class="col-sm-4">Phone #:</div>
                            <div class="col-sm-8">
                                <input id="phonenum" name="phonenum" type="text" class="form-control input-sm">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-12 bordered vert-separate">
                        <div class="row">
                            <em style="text-decoration: underline;">Input:</em>
                            <div class="col-sm-5">
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Scans</div>
                                    <input id="invoice-num" name="invoice-num" class="form-control" type="text">
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Supplied Media</div>
                                    <input id="invoice-num" name="invoice-num" class="form-control" type="text">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- COL RIGHT -->
            <div class="col-xs-6">
                <div class="row">
                    <div class="col-sm-11 col-sm-offset-1 bordered vert-separate">
                        <div class="row">
                            <div class="col-sm-5">In Production Ship Proofs</div>
                            <div class="col-sm-7">
                                <input id="customer" name="customer" type="text" class="form-control input-sm">
                                <input id="customer" name="customer" type="text" class="form-control input-sm">
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-sm-4">Ship Job:</div>
                            <div class="col-sm-8">
                                <input id="projectname" name="projectname" type="text" class="form-control input-sm">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-11 col-sm-offset-1 bordered vert-separate">

                        <div class="row">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <em style="text-decoration: underline;">Proof Address:</em>

                            <div class="form-group">
                                <div class="col-sm-11 col-sm-offset-1">
                                    <textarea id="contact" rows="3" type="text"
                                              class="form-control input-sm"></textarea>
                                </div>
                                <div class="col-sm-11 col-sm-offset-1">
                                    <br/><textarea id="contact" rows="3" type="text"
                                                   class="form-control input-sm"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: ROW (2 columns)-->



        <div class="row">
                <table id="mainTable" border="1">

                    <tr style="font-weight:bold; text-align: right;">
                        <td class="">Vinyl</td>
                        <td class="">Lam</td>
                        <td class="">Name</td>
                        <td class="">Description</td>
                        <td class="">Unit_Price</td>
                        <td class="">Total_Price</td>
                        <td class="">Part_#</td>
                        <td class="">Width</td>
                        <td class="">Height</td>
                        <td class="">Qty</td>
                    </tr>

                    <?php for ($i = 0; $i < 15; $i++) { ?>
                        <tr style="font-weight:bold; text-align: right;">
                            <td data-what="main-vinyl-<?php echo $i; ?>">&nbsp;</td>
                            <td data-what="main-lam-<?php echo $i; ?>">&nbsp;</td>
                            <td data-what="main-name-<?php echo $i; ?>">&nbsp;</td>
                            <td data-what="main-desc-<?php echo $i; ?>">&nbsp;</td>
                            <td data-what="main-uniprice-<?php echo $i; ?>">&nbsp;</td>
                            <td data-what="main-totprice-<?php echo $i; ?>">&nbsp;</td>
                            <td data-what="main-partnum-<?php echo $i; ?>">&nbsp;</td>
                            <td data-what="main-width-<?php echo $i; ?>">&nbsp;</td>
                            <td data-what="main-height-<?php echo $i; ?>">&nbsp;</td>
                            <td data-what="main-qty-<?php echo $i; ?>">&nbsp;</td>
                        </tr>
                    <?php } ?>
                </table>
            <!-- Submit --><br/>
            <div class="form-group">
                <div class="text-center">
                    <button id="submit-wo" name="submit-wo" class="btn btn-primary">
                        Submit Production Work Order
                    </button>
                    <div>[VISUAL IMPRESSIONS]</div>
                </div>
            </div>
        </div>
    </div>



<br/><br/><br/>

</form>
<script>
    $('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();
    $('#textAreaEditor').editableTableWidget({editor: $('<textarea>')});
</script>


</body>
</html>