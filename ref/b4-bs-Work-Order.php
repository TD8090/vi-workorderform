<?php

include('../../lib/config.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>VI - Work Order Form</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<meta name="author" content="John Brittain III">
	<meta name="robots" content="NOINDEX,NOFOLLOW">
	
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/cp/styles/imgs/favicon144x144.gif">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/cp/styles/imgs/favicon114x114.gif">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/cp/styles/imgs/favicon72x72.gif">
                  <link rel="apple-touch-icon-precomposed" href="/cp/styles/imgs/favicon57x57.gif">
                                 <link rel="shortcut icon" href="/cp/styles/imgs/favicon.gif">
	
	
	<style type="text/css">
	
body {margin:0; padding:0;}
#wrapper {width:80%; margin:0 auto; border:1px solid #000; border-width:0 1px; padding:5%;}
table {width:99%; border-collapse:collapse; margin-top:20px;}
td {padding:3px; vertical-align: middle;}
.header {vertical-align: top;}
.comments {border:1px solid #000; padding:5px; margin:5px;}
.center-block {text-align:center;}
.center-table {width:70%; margin:20px auto;}
/*.bordered tr {border:1px solid #000; border-width: 1px 0;} */
.bordered {border-width: 0 0 1px 1px; border-spacing: 0; border-color:#000; border-collapse: collapse; border-style: solid;}
.bordered td, .bordered th {padding: 4px; border-width: 1px 1px 0 0; border-color:#000; border-style: solid; height:25px; width:32%; text-align:left;}

.bordered thead th {text-align:center;}

.main-table {width:90%; margin:20px auto;}

.text-right {text-align:right;}
.text-box {width:98%;}
	</style>
</head>
<body>
<div id="wrapper">
	<table class="header">
		<tr>
			<td>logo</td>
			<td><h1 class="text-right">Work Order</h1></td>
		</tr>
	</table>
	
	<table class="second">
		<tr>
			<td>
				606 Hebron Street<br>
				Charlotte NC<br>
				Phone 704.525.0190<br>
				Fax 704.525.0690 
			</td>
			<td class="text-right">
				INVOICE #<span class="editable invoice-num" data-what="invoice-number">[100]</span><br>
				DATE: <span class="editable date-picker" data-what="date">[PICK THE DATE]</span>
			</td>
		</tr>
	</table>
	
	<table class="address">
		<tr>
			<td>
				<strong>CLIENT INFO:</strong><br>
				[Name]<br>[Company Name]<br>[Street Address]<br>[City, ST ZIP Code]<br>[Phone Number] 
			</td>
			<td>
				<strong>SHIPPING INFO:</strong><br>
				[Name]<br>[Company Name]<br>[Street Address]<br>[City, ST ZIP Code]<br>[Phone Number] 
			</td>
		</tr>
	</table>
	
	<table class="address">
		<tr>
			<td>
				<strong>JOB DESCRIPTION/SPECIAL COMMENTS </strong>
			</td>
		</tr>
		<tr>
			<td class="comments"></td>
		</tr>
	</table>
	
	<div class="center-block">
		<table class="center-table bordered">
			<thead>
				<tr>
					<th>ASSINED TO:</th><th>START DATE</th><th>DUE DATE</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="editable" data-what="assigned-to-1">&nbsp;</td>
					<td class="editable" data-what="assigned-start-1">&nbsp;</td>
					<td class="editable" data-what="assigned-due-1">&nbsp;</td>
				</tr>
			</tbody>
		</table>
		
		<table class="main-table bordered">
			<thead>
				<tr>
					<th>PARTS LIST</th><th>DESCRIPTION</th><th>QUANTITY</th>
				</tr>
			</thead>
			<tbody>
				<?php for($i=0;$i<15;$i++){ ?>
				<tr>
					<td class="editable" data-what="main-parts-<?php echo $i; ?>">&nbsp;</td>
					<td class="editable" data-what="main-desc-<?php echo $i; ?>">&nbsp;</td>
					<td class="editable" data-what="main-quantity-<?php echo $i; ?>">&nbsp;</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		
		<div class="text-center">
			[VI LOGO]
		</div>
	</div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
	<script>window.jQuery || document.write('<script src="/cp/styles/jquery.1.10.2.js"><\/script>')</script>
	<script type="text/javascript" src="/cp/styles/jquery.form.js"></script>
	<script type="text/javascript" src="/cp/styles/plugins/autoGrow.js"></script>
<script type="text/javascript">

$(function(){
	var $editing = false, box = {}, boxes =  [], editable = $('.editable')

	$('#save_puur').on('click', function(){
		editable.each(function(){
			var th = $(this), what = th.data('what'), input = th.find('input'), val
			if(input.length){
				if(what == 'r-date'){
//					alert('gotta add the sups back to the date!')
					val = input.val()
					val = val.replace(/(\d+)+(st|nd|rd|th)([^\w\d]|$)/g, "$1<sup>$2</sup>$3")
				} else {
					val = input.val()
				}
				th.html(val)
			}
			box = {
				what : what,
				data : th.html()
			}
			boxes.push(box)
		})
		$.post('spuur.gen.php', {values : boxes}, function(r){
			$('#results').html(r)
			window.location = '<?php echo $_SERVER['PHP_SELF']; ?>?download'
		})
//		console.log(boxes)
	})
	
	editable.on('click', function(){
		var th = $(this), what = th.data('what'), inner = th.html().replace('"', "'")
		if(th.find('input').length){
//			alert('has input')
		} else {
			if($editing != false){
				// they're currently editing a line so we gonna update that line.
				var input = $('#wrapper input'), val = input.val(), parent = input.parent()
				if($editing == 'r-date'){
					val = val.replace(/(\d+)+(st|nd|rd|th)([^\w\d]|$)/g, "$1<sup>$2</sup>$3")
				}
				parent.html(val)
			}
			if(what == 'r-date'){
				inner = inner.replace(/<sup>/g, '')
				inner = inner.replace(/<\/sup>/g, '')
			}
			$editing = what
			th.html('<input type="text" value="'+inner+'" class="text-box">')
			th.find('input').blur().focus()
		}
	}).each(function(ind, val){
		$(this).data('index',ind)
	})
	$(document).delegate('input', 'keypress', function(e){
		var code = e.keyCode || e.which
		if(code == 13 || code == 9){
			e.preventDefault();
			var th = $(this), val = th.val(), par = th.parent('.editable'), what = par.data('index')
			if(par.data('what') == 'r-date'){
				val = val.replace(/(\d+)+(st|nd|rd|th)([^\w\d]|$)/g, "$1<sup>$2</sup>$3")
			}
			par.html(val)
			$editing = false
			$('.editable').eq(parseInt(what)+1).trigger('click')
		}
	})/* was trying to clear the input field if found.. but freezes everything.. 
	.on('click', function(){
		var input = $('input')
		if(input.length){ // input found
			// lets hide that input..
			input.parent().html(input.val())
		}
	})
	*/
})
</script>
</body>
</html>