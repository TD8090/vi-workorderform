<?php
// setting the session lifetime to 1 hour because of ppl b*tchin.
// and allowing the sessions across subdomains.. ie. www.vi-digital.com and vi-digital.com

header('Content-Type: text/html; charset=utf-8');

// to gzip the site / save on bandwidth.
ob_start("ob_gzhandler");

$Sets['session_length'] = (60*60*9);
$Sets['domain'] = 'vi-digital.com';
session_name('VI_CP');
ini_set('session.gc_maxlifetime', $Sets['session_length']);
ini_set('session.cookie_domain', '.'.$Sets['domain']);
session_set_cookie_params($Sets['session_length'], '/', '.'.$Sets['domain']);

//ini_set("suhosin.session.cryptdocroot", "Off");
//ini_set("suhosin.cookie.cryptdocroot", "Off");

session_start();
//header("X-UA-Compatible: IE=Edge");
define('VI', 'menuboards', true);

// database info is also found in /lib/getfeed.php for the webtrition portion
$Sets['db']['server'] = '10.208.37.93';
$Sets['db']['table'] = 'menuboards'; //'visual_impressions'; //'menuboards';
$Sets['db']['user'] = 'root'; //'root';
$Sets['db']['pass'] = '8vxRwJtjfv0T0M4wFWE3'; //'8vxRwJtjfv0T0M4wFWE3';

// lets connect to mysql
$conn = mysql_connect($Sets['db']['server'], $Sets['db']['user'], $Sets['db']['pass']) or die('Error establisting a connection to the database.');
mysql_select_db($Sets['db']['table'], $conn) or die('Error connecting to database table.');
mysql_query("SET NAMES utf8");
unset($Sets['db']);

$Sets['url_root'] = 'cp.php';
$Sets['path'] = '/cp/';

$Sets['date_format_long'] = 'D M jS, Y';
$Sets['date_format_time'] = 'm/d/Y ~ g:i a';
$Sets['date_format_day'] = 'm/d/Y';

$Sets['tickets']['priority'] = array('Low', 'Medium', 'High', 'Urgent', 'Resolved'); // resolved is set by the script.
$Sets['tickets']['cats'] = array('Training', 'Sales', 'Support', 'Delivery', 'New Promo / LTO'); //, 'General', 'Networking', 'Scheduling');

// client ids that are within compass group.
$Sets['compass-group'] = array('0',9,23,7,30);

$Sets['hdmi-1920x1080'] = array('JWU Denver', 'Nex Dine Kiosks', 'Corporate - Salsaritas', 'RDU', 'matts');

$Sets['webtrition-name-change'] = array('Worcester', 'University of Miami');

if(isset($_SESSION['vi']) && $_SESSION['vi']['utype'] == 'employee'){
	$Sets['tickets']['cats'][] = 'Traveling Ticket';
	$Sets['tickets']['cats'][] = 'New Job';
	$Sets['tickets']['cats'][] = 'New Creative';
	$Sets['tickets']['cats'][] = 'Reports';
	$Sets['tickets']['cats'][] = 'Software Bugs';
	$Sets['tickets']['cats'][] = 'Website Bugs';
//	$Sets['tickets']['cats'][] = ;
}

if(!isset($stop_includes)){
	$inc_files = array(
		'Players.php',
		'Locations.php',
		'location_info.php',
		'us.php',
		'Tickets.php',
		'Library.php',
		'Comments.php',
		'Clients.php',
		'Schedule.php',
		'utilities.php',
		'vi-files.php',
		'ticket_assign.php',
		'playlist.php'
	);
	foreach($inc_files as $file){
		require_once($_SERVER['DOCUMENT_ROOT'].$Sets['path'].'/classes/'.$file);
	}
}

// functions.
function es($str){
	return mysql_real_escape_string($str);
}

function mail2($to, $subject, $message, $header){
	if(@mail($to, $subject, $message, $header)){
		return true;
	} else { 
		return false;
	}
}

function militaryTime($clock){
  if(preg_match("/am$/i", $clock)) {
    $clock = str_ireplace("am", "", $clock);
    $clock = explode(":", $clock);
//    if ($clock[0] < 10) $clock[0] = '0'.$clock[0];
    if ($clock[0] == 12) $clock[0] = '00';
    return $clock[0].$clock[1];
  } else if (preg_match("/pm$/i", $clock)){
    $clock = str_ireplace("pm", "", $clock);
    $clock = explode(":", $clock);
    if ($clock[0] < 12) $clock[0] = $clock[0]+12;
    return $clock[0].$clock[1];
  }
}

function quickForm($id, $txt, $val, $ph='', $new_help=''){
	//if($id == 'ustate' || $id == 'ustate1'){
	if(in_array($id, array('ustate', 'ustate1', 'usstate'))){
		$input = '<select name="'.$id.'" id="'.$id.'">';
		$state_list = array('AL'=>"Alabama",'AK'=>"Alaska", 'AZ'=>"Arizona",'AR'=>"Arkansas", 'CA'=>"California", 'CO'=>"Colorado",'CT'=>"Connecticut",  'DE'=>"Delaware",  'DC'=>"District Of Columbia", 'FL'=>"Florida", 'GA'=>"Georgia", 'HI'=>"Hawaii", 'ID'=>"Idaho", 'IL'=>"Illinois", 'IN'=>"Indiana", 'IA'=>"Iowa", 'KS'=>"Kansas", 'KY'=>"Kentucky", 'LA'=>"Louisiana", 'ME'=>"Maine", 'MD'=>"Maryland", 'MA'=>"Massachusetts", 'MI'=>"Michigan", 'MN'=>"Minnesota", 'MS'=>"Mississippi", 'MO'=>"Missouri", 'MT'=>"Montana", 'NE'=>"Nebraska", 'NV'=>"Nevada", 'NH'=>"New Hampshire", 'NJ'=>"New Jersey", 'NM'=>"New Mexico", 'NY'=>"New York", 'NC'=>"North Carolina", 'ND'=>"North Dakota", 'OH'=>"Ohio", 'OK'=>"Oklahoma", 'OR'=>"Oregon", 'PA'=>"Pennsylvania", 'RI'=>"Rhode Island", 'SC'=>"South Carolina", 'SD'=>"South Dakota", 'TN'=>"Tennessee", 'TX'=>"Texas", 'UT'=>"Utah", 'VT'=>"Vermont", 'VA'=>"Virginia", 'WA'=>"Washington", 'WV'=>"West Virginia", 'WI'=>"Wisconsin", 'WY'=>"Wyoming");
		foreach($state_list as $k => $v){
			$add = $val == $k ? ' selected="selected"' : '';
			$input .= '<option value="'.$k.'"'.$add.'>'.$v.'</option>';
		}
		$input .= '</select>';
		$help = '';
	} else {
		$type = $ph == 'Password' ? 'password' : 'text';
		
		$input = '<input class="span12" type="'.$type.'" name="'.$id.'" id="'.$id.'" value="'.htmlentities($val).'" placeholder="'.$ph.'">';
	}
	if($id == 'npass2'){
		$help = '<span class="help-block">If you\'re just changing your email address, you can leave the new password fields blanks, but your current password is still required.</span>';
	} else {
		$help = '';
	}
	if($new_help != ''){
		$help = '<span class="help-block">'.$new_help.'</span>';
	}
	return '<div class="row-fluid">
		<div class="span3 text-right">
			<label for="'.$id.'">'.$txt.':</label>
		</div>
		<div class="span9">'.$input.$help.'</div>
	</div>';
}

// functions below were made by bobby.
// is this one used?.. need to do a search, but i doubt it..
// needing to redo it anyways.. [update] redo is above.
function getMilitaryTime ($clock){
  if(preg_match("/am$/i", $clock)) {
    $clock = str_ireplace("am", "", $clock);
    $clock = explode(":", $clock);
    if ($clock[0] < 10) $clock[0] = '0'.$clock[0];
    elseif ($clock[0] == 12) $clock[0] = '00';
    return $clock[0].$clock[1];
  } else if (preg_match("/pm$/i", $clock)){
    $clock = str_ireplace("pm", "", $clock);
    $clock = explode(":", $clock);
    if ($clock[0] < 12) $clock[0] = $clock[0]+12;
    elseif ($clock[0] == 12) $clock[0] = '13';
    return $clock[0].$clock[1];
  }
}

function timetostr($time, $specific=false){
  if ($time < 60)
    $time = $time.' sec.';
  elseif ($time < 3600){
    $min = floor($time/60);
    $sec = $time - ($min*60);
    if ($sec < 10) $sec = '0'.$sec;
    $time = $min.' min';
    if ($specific) $time .= ', '.$sec.' sec';
  } elseif ($time < 86400){
    $hr = floor($time/3600);
    $min = floor(($time - ($hr*3600)) / 60);
    $sec = $time - (($min*60)+($hr*3600));
    if ($sec < 10) $sec = '0'.$sec;
    $time = $hr.' hr.';
  } else {
    $day = floor($time/86400);
    $hr = floor(($time/3600) - ($day * 24));
    $min = floor(($time - ($hr*3600) - ($day*86400)) / 60);
    $sec = $time - (($min*60)+($hr*3600)+($day*86400));
    if ($sec < 10) $sec = '0'.$sec;
    if ($day == 1) $time = 'Yesterday';
    else $time = $day.' days';
  }
	if ($time != 'Yesterday') $time = $time . ' ago';
  return $time;
}


function makeimages($img_src, $filename, $picw=640, $pich=480, $stretch=0){
	list(, , $type, ) = getimagesize($img_src);
	if($type == 1){  # GIF IMAGE
		$ext = "gif";
		$src_img = imagecreatefromgif($img_src);
	} elseif($type == 2){ # JPEG IMAGE
		$ext = "jpg";
		$src_img = imagecreatefromjpeg($img_src);
	} elseif($type == 3){ #PNG IMAGE
		$ext = "png";
		$src_img = imagecreatefrompng($img_src);
	}
	$imgw = imagesx($src_img);
	$imgh = imagesy($src_img);
	if($imgw > $picw || $imgh > $pich){
		if($imgw > $imgh){
			$aspect = $imgh / $imgw;
			$neww = $picw;
			$newh = round($neww * $aspect);
		} elseif($imgw < $imgh){
			$aspect = $imgw / $imgh;
			$newh = $pich;
			$neww = round($newh * $aspect);
		} elseif ($imgw == $imgh){
			$neww = $picw;
			$newh = $pich;
		}
	} else {
		if($stretch){
			if ($imgw > $imgh){
				$aspect = $imgh / $imgw;
				$neww = $picw;
				$newh = round($neww * $aspect);
			} elseif ($imgw < $imgh) {
				$aspect = $imgw / $imgh;
				$newh = $pich;
				$neww = round($newh * $aspect);
			} elseif ($imgw == $imgh) {
				$neww = $picw;
				$newh = $pich;
			}
		}
		$neww = $imgw;
		$newh = $imgh;
	}
	$dst_img = imagecreatetruecolor($neww, $newh);
	imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $neww, $newh, $imgw, $imgh);

	if($type == 1){
		imagegif($dst_img, $filename);
	} elseif($type == 2){
		imagejpeg($dst_img, $filename, 75);
	} elseif($type == 3){
		imagepng($dst_img, $filename, 7);
	}
	return $filename; //.".".$ext;
}

?>
