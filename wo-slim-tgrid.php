<!DOCTYPE html><html lang="en">
<head>
    <meta charset="utf-8">
    <title>VI - Work Order Form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!--BOOTSTRAP-->
    <link href="src/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!--<script src="src/bootstrap/js/bootstrap.min.js"></script>-->
<!--JQUERY-->
    <script src="src/jquery-2.1.0.min.js" type="text/javascript"></script>
<!--JQUERY-UI-->
    <link rel="stylesheet" href="src/jquery-ui-1.11.4/jquery-ui.min.css" type="text/css" media="screen">
    <script src="src/jquery-ui-1.11.4/jquery-ui.min.js"></script>

<!-- editable-grid -->
    <script src="src/editablegrid-master/editablegrid_editors.js"></script>
<!--.....USER.....-->
    <link href="wo-slim.css" rel="stylesheet">
    <!--    <script src="wo-slim.js"></script>-->

</head>
<body>

<form class="page-container">
    <div class="container">

            <fieldset class="row bordered">

                    <h1 class="col-xs-4">Production Work Order</h1>

                    <div class="col-xs-5">
                        <label class="control-label" for="sigsreqd">Signatures Required:</label>
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-xs btn-default active">
                                <input type="radio" name="sigsreqd" id="sigsreqd1" autocomplete="off" checked>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </label>
                            <label class="btn btn-xs btn-default">
                                <input type="radio" name="sigsreqd" id="sigsreqd2" autocomplete="off">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </label>
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <div class="input-group">
                            <span class="input-group-addon">JOB#</span>
                            <input id="prependedtext" name="invoice-num" class="form-control" type="text">
                        </div>
                    </div>

            </fieldset><!-- end: ROW (header)--><br/>


        <div class="row"><!-- ROW (2 COLS) -->

            <div class="col-xs-6 column">
                <div class="row">
                    <form class="form-inline">
                        <div class="form-group">
                            <label class="col-xs-4 control-label" for="textinput">Customer</label>

                            <div class="col-xs-8">
                                <input id="Customer" name="textinput" type="text" class="form-control input-sm"
                                       placeholder="placeholder">
                            </div>
                            <label class="col-xs-4 control-label" for="projectname">Project Name</label>

                            <div class="col-xs-8">
                                <input id="projectname" name="projectname" type="text" class="form-control input-sm">
                            </div>
                            <label class="col-xs-4 control-label" for="address">Address</label>

                            <div class="col-xs-8">
                                <input id="address" name="address" type="text" class="form-control input-sm">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ProjectName">Project Name</label>
                            <input type="text" class="form-control" id="ProjectName" placeholder="Project Name">
                        </div>
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox1" value="option1"> 1
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox2" value="option2"> 2
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox3" value="option3"> 3
                        </label>
                    </form>
                </div>
                <table class="table table-condensed table-bordered">
                    <tr>
                        <td class="col-xs-2">Customer</td>
                        <td class="col-xs-2 editable" data-what="address" data-type=""></td>
                    </tr>
                    <tr>
                        <td class="col-xs-2">Project Name</td>
                        <td class="col-xs-2 editable" data-what="address" data-type=""></td>
                    </tr>
                    <tr>
                        <td class="col-xs-2">Address</td>
                        <td class="col-xs-2 editable" data-what="address" data-type=""></td>
                    </tr>
                    <tr>
                        <td class="col-xs-2">Sales Rep</td>
                        <td class="col-xs-2 editable" data-what="address" data-type=""></td>
                    </tr>
                    <tr>
                        <td class="col-xs-2">Contact</td>
                        <td class="col-xs-2 editable" data-what="address" data-type=""></td>
                    </tr>
                    <tr>
                        <td class="col-xs-2">Phone Number</td>
                        <td class="col-xs-2 editable" data-what="address" data-type=""></td>
                    </tr>
                    <tr>
                        <td class="col-xs-2">Scans</td>
                        <td class="col-xs-2 editable" data-what="address" data-type=""></td>
                    </tr>
                    <tr>
                        <td class="col-xs-2">Supplied Media</td>
                        <td class="col-xs-2 editable" data-what="address" data-type=""></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="col-xs-2 checkbox chkbox pull-left" data-what="232-chkbox"></div>
                            Retrieval
                        </td>
                        <td>
                            <div class="col-xs-2 checkbox chkbox pull-left" data-what="313-chkbox"></div>
                            Email
                        </td>
                    </tr>
                    <tr>
                        <td class="editable" data-what="date" data-type="textarea">
                            <div class="pull-right">
                                Job #
                                <col-xs- class="editable invoice-num" data-what="invoice-number">[100]</col-xs->
                                <br/>
                                DATE:
                                <col-xs- class="editable date-picker" data-what="date" data-type="date">[PICK THE DATE]
                                </col-xs->
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

            <!-- COL RIGHT -->
            <div class="col-xs-6 column">
                <table class="table table-condensed">
                    <tr>
                        <td class="editable" data-what="client-info" data-type="location">
                            <strong>CLIENT INFO:</strong><br>
                            [Name]<br>[Company Name]<br>[Street Address]<br>[City, ST ZIP Code]<br>[Phone Number]
                        </td>
                        <td class="editable" data-what="shipping-info" data-type="location">
                            <strong>SHIPPING INFO:</strong><br>
                            [Name]<br>[Company Name]<br>[Street Address]<br>[City, ST ZIP Code]<br>[Phone Number]
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <!-- end: ROW (2 columns)-->


        <div class="row">
            <table class="table table-condensed no-border">
                <tr>
                    <td>
                        <strong>JOB DESCRIPTION/SPECIAL COMMENTS </strong>
                    </td>
                </tr>
                <tr>
                    <td class="comments editable" data-what="job-comments" data-type="textarea">comments here..</td>
                </tr>
            </table>
        </div>

        <div class="row-fluid">
            <div class="col-xs-10 offset1">
                <div class="row-fluid">
                    <table class="table table-bordered assigned">
                        <thead>
                        <tr>
                            <th class="col-xs-3">ASSIGNED TO:</th>
                            <th class="col-xs-3">START DATE</th>
                            <th class="col-xs-3">DUE DATE</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="editable" data-what="assigned-to-1" data-type="textarea">&nbsp;</td>
                            <td class="editable" data-what="assigned-start-1" data-type="date">&nbsp;</td>
                            <td><input id="datepicker" class="editable" data-what="assigned-due-1" data-type="date" />&nbsp;</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row-fluid">
            <table id="mainTable" border="1">
                <thead>
                <tr>
                    <th class="col-xs-1">Vinyl</th>
                    <th class="col-xs-1">Lam</th>
                    <th class="col-xs-2">Name</th>
                    <th class="col-xs-2">Description</th>
                    <th class="col-xs-1">Unit Price</th>
                    <th class="col-xs-1">Total Price</th>
                    <th class="col-xs-1">Part #</th>
                    <th class="col-xs-1">W</th>
                    <th class="col-xs-1">H</th>
                    <th class="col-xs-1">Qty</th>
                </tr>
                </thead>
                <tbody>
                <?php for ($i = 0; $i < 15; $i++) { ?>
                    <tr>
                        <td><div data-what="main-vinyl-<?php echo $i; ?>">&nbsp;</div></td>
                        <td><div data-what="main-lam-<?php echo $i; ?>">&nbsp;</div></td>
                        <td><div data-what="main-name-<?php echo $i; ?>">&nbsp;</div></td>
                        <td><div data-what="main-desc-<?php echo $i; ?>">&nbsp;</div></td>
                        <td><div data-what="main-uniprice-<?php echo $i; ?>">&nbsp;</div></td>
                        <td><div data-what="main-totprice-<?php echo $i; ?>">&nbsp;</div></td>
                        <td><div data-what="main-partnum-<?php echo $i; ?>">&nbsp;</div></td>
                        <td><div data-what="main-width-<?php echo $i; ?>">&nbsp;</div></td>
                        <td><div data-what="main-height-<?php echo $i; ?>">&nbsp;</div></td>
                        <td><div data-what="main-qty-<?php echo $i; ?>">&nbsp;</div></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>


<br/>
<!-- Submit -->
<div class="form-group">
    <div class="text-center">
        <button id="submit-workorder" name="submit-workorder"
                class="btn btn-primary text-center">Submit Production Work Order
        </button>
        <div>[VISUAL IMPRESSIONS]</div>
    </div>
</div>
<br/><br/><br/>

</form>
<script>
/*
    $('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();
    $('#textAreaEditor').editableTableWidget({editor: $('<textarea>')});
*/

    $(function() {
        $( "#datepicker" ).datepicker();
    });
</script>
<script>
    $(function () {
        $("td").dblclick(function () {
            var OriginalContent = $(this).text();

            $(this).addClass("cellEditing");
            $(this).html("<input type="text" value="&quot; + OriginalContent + &quot;" />");
            $(this).children().first().focus();

            $(this).children().first().keypress(function (e) {
                if (e.which == 13) {
                    var newContent = $(this).val();
                    $(this).parent().text(newContent);
                    $(this).parent().removeClass("cellEditing");
                }
            });

            $(this).children().first().blur(function(){
                $(this).parent().text(OriginalContent);
                $(this).parent().removeClass("cellEditing");
            });
        });
    });

    /*
     Read more: http://www.ssiddique.info/jquery-and-editable-html-table.html#ixzz3ZVfvx5gi
     */
</script>
</body>
</html>